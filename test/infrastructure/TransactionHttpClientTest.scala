package infrastructure

import domain.CustomerId
import domain.external.TransactionServiceClientError
import helpers.TestDataGen
import infrastructure.http.TransactionHttpClient
import org.scalatest.concurrent.ScalaFutures.whenReady
import org.scalatest.concurrent.Waiters.timeout
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.{BuiltInComponentsFromContext, Configuration}
import play.api.mvc.Results
import play.api.routing.Router
import play.api.test.{Injecting, WsTestClient}
import play.filters.HttpFiltersComponents
import play.api.routing.sird._
import play.core.server.Server

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt

class TransactionHttpClientTest extends PlaySpec with Injecting with GuiceOneAppPerTest {

  "Transaction http client" should {
    "return transaction by customer id" in {
      val config = inject[Configuration]

      withTransactionClient(config) { client =>
        whenReady(client.getTransactionsByCustomer(CustomerId(123456)),
                  timeout(1.seconds)) { result =>
          result.isRight mustBe true
        }
      }
    }

    "translate error from server" in {
      val config = inject[Configuration]

      withTransactionClient(config) { client =>
        whenReady(client.getTransactionsByCustomer(CustomerId(1)), timeout(1.seconds)) {
          result =>
            println(result)
            result.isLeft mustBe true
            result.left.getOrElse("") mustBe TransactionServiceClientError(
              "HTTP request rejected with status code: 500 Internal Server Error")
        }
      }
    }

    "save transaction" in {
      val config = inject[Configuration]
      withTransactionClient(config) { client =>
        whenReady(client.sendTransaction(TestDataGen.transactionSample),
                  timeout(1.seconds)) { result =>
          result.isRight mustBe true
        }
      }
    }

  }

  def withTransactionClient[T](configuration: Configuration)(
      block: TransactionHttpClient => T): T = {
    Server.withApplicationFromContext() { context =>
      new BuiltInComponentsFromContext(context) with HttpFiltersComponents {
        override def router: Router = Router.from {
          case GET(p"/transactions/customer/123456") =>
            Action { _ =>
              Results.Ok
                .sendResource("transactions-by-customer.json")(executionContext,
                                                               fileMimeTypes)
            }
          case GET(p"/transactions/customer/1") =>
            Action { _ =>
              Results.InternalServerError
            }
          case POST(p"/transaction") =>
            Action { _ =>
              Results.Ok.sendResource("transaction-result.json")(executionContext,
                                                                 fileMimeTypes)
            }
        }
      }.application
    } { implicit port =>
      WsTestClient.withClient { client =>
        block(new TransactionHttpClient(client, configuration))
      }
    }
  }
}
