package infrastructure

import domain.{Customer, CustomerFullName, CustomerId}
import org.scalatest.concurrent.ScalaFutures.whenReady
import org.scalatestplus.play.PlaySpec
import play.api.Configuration

class InMemoryCustomerRepositoryTest extends PlaySpec {
  val testConfig: Configuration =
    Configuration.apply(("customers.data.path", "test/data/customers.json"))

  "In Memory Customer Repository" should {

    "load initial data as a Map" in {
      val inMemoryCustomer = new InMemoryCustomerRepository(testConfig)
      val customersMap = inMemoryCustomer.loadCustomers("test/data/customers.json")

      customersMap mustBe Map(
        123456 -> Customer(CustomerId(123456), CustomerFullName("Peter", "Pan")),
        654321 -> Customer(CustomerId(654321), CustomerFullName("Lucy", "Liu"))
      )
    }

    "get customer by ID" in {
      val inMemoryCustomer = new InMemoryCustomerRepository(testConfig)
      val customerFuture = inMemoryCustomer.customerById(CustomerId(123456))

      whenReady(customerFuture) { customer =>
        customer mustBe Some(
          Customer(CustomerId(123456), CustomerFullName("Peter", "Pan"))
        )
      }
    }

    "return None when a customer by ID is not found" in {
      val inMemoryCustomer = new InMemoryCustomerRepository(testConfig)
      val customerFuture = inMemoryCustomer.customerById(CustomerId(8797))

      whenReady(customerFuture) { customer =>
        customer mustBe None
      }
    }

    "get all customers" in {
      val inMemoryCustomer = new InMemoryCustomerRepository(testConfig)
      val customersFuture = inMemoryCustomer.allCustomers

      whenReady(customersFuture) { customers =>
        customers mustBe List(
          Customer(CustomerId(123456), CustomerFullName("Peter", "Pan")),
          Customer(CustomerId(654321), CustomerFullName("Lucy", "Liu"))
        )
      }
    }

  }

}
