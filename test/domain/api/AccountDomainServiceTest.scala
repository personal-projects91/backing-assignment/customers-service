package domain.api

import domain.CustomerId
import helpers.{ServicesBuilder, TestDataGen}
import org.scalatest.concurrent.ScalaFutures.whenReady
import org.scalatestplus.play.PlaySpec

import scala.concurrent.ExecutionContext.Implicits.global

class AccountDomainServiceTest extends PlaySpec {

  "Account Domain Service" should {
    "handle incoming open account command" in {
      val accountService = ServicesBuilder.buildAccountDomainService
      val result = accountService.openAccount(TestDataGen.currentAccount)

      whenReady(result) { result =>
        result mustBe Right(TestDataGen.currentAccount)
      }
    }

    "return error when the customer id doesn't exit" in {
      val accountService = ServicesBuilder.buildAccountDomainService
      val result = accountService.openAccount(
        TestDataGen.currentAccount.copy(customerId = CustomerId(5555)))

      whenReady(result) { result =>
        result mustBe Left(AccountHandlerError(s"Customer id not found: ${5555}"))
      }
    }
  }

}
