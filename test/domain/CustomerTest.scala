package domain

import org.scalatestplus.play.PlaySpec
import play.api.libs.json.Json

import scala.io.Source

class CustomerTest extends PlaySpec {

  "Customer domain entity" should {

    "be able to build from json" in {
      val jsonFile = Source.fromFile("test/data/customers.json")
      val json = Json.parse(jsonFile.mkString)
      jsonFile.close()

      val customerList = json.as[List[Customer]]

      customerList mustBe List(
        Customer(CustomerId(123456), CustomerFullName("Peter", "Pan")),
        Customer(CustomerId(654321), CustomerFullName("Lucy", "Liu"))
      )

    }

    "be able to write json" in {
      val customer = Customer(CustomerId(123456), CustomerFullName("Peter", "Pan"))

      val jsonResult = Json.toJson(customer)

      jsonResult.toString() mustBe """{"id":123456,"name":"Peter","surName":"Pan"}"""

    }

  }

}
