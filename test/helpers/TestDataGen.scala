package helpers

import java.time.Instant
import java.util.UUID

import domain.{AccountId, CurrentAccount, CustomerId, Money, Transaction, TransactionId}

object TestDataGen {
  val currentAccount: CurrentAccount = CurrentAccount(CustomerId(123456), Money(56.3))()
  val transactionSample: Transaction = {
    Transaction(
      Some(TransactionId(UUID.fromString("a32b258f-b75d-4b85-9f06-8aa1d46438f4"))),
      CustomerId(123),
      AccountId(456),
      Money(52.00),
      Some(Instant.EPOCH)
    )
  }
}
