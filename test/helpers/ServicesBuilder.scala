package helpers

import domain.{AccountDomainService, CustomerId, Transaction}
import domain.external.{TransactionServiceClient, TransactionServiceClientError}
import infrastructure.{InMemoryAccountRepository, InMemoryCustomerRepository}
import play.api.Configuration

import scala.concurrent.{ExecutionContext, Future}

object ServicesBuilder {

  def buildAccountDomainService: AccountDomainService = {
    val testConfig =
      Configuration.apply(("customers.data.path", "test/data/customers.json"))
    val mockTransactionService = new MockTransactionService
    val inMemoryAccountRepository = new InMemoryAccountRepository
    val inMemoryCustomerRepository = new InMemoryCustomerRepository(testConfig)
    new AccountDomainService(inMemoryAccountRepository,
                             inMemoryCustomerRepository,
                             mockTransactionService)
  }

  class MockTransactionService extends TransactionServiceClient {
    override def sendTransaction(transaction: Transaction)(implicit ex: ExecutionContext)
      : Future[Either[TransactionServiceClientError, Transaction]] =
      Future.successful(Right(TestDataGen.transactionSample))

    override def getTransactionsByCustomer(customerId: CustomerId)(
        implicit ex: ExecutionContext)
      : Future[Either[TransactionServiceClientError, List[Transaction]]] =
      Future.successful(Right(List(TestDataGen.transactionSample)))
  }
}
