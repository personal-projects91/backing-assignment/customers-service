package controllers

import helpers.ServicesBuilder
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.libs.json.Json
import play.api.test.Helpers.{
  CONTENT_TYPE,
  POST,
  defaultAwaitTimeout,
  status,
  stubControllerComponents
}
import play.api.test.{FakeHeaders, FakeRequest, Injecting}
import play.api.http.Status.{BAD_REQUEST, OK}

import scala.concurrent.ExecutionContext.Implicits.global

class AccountControllerTest extends PlaySpec with GuiceOneAppPerTest with Injecting {

  val accountService = ServicesBuilder.buildAccountDomainService

  "AccountController POST /account/current" should {

    "handle new account and return 200" in {
      val controller =
        new AccountController(accountService, stubControllerComponents())

      val fakeRequest = FakeRequest(POST, "/account/current")
        .withHeaders(FakeHeaders(Seq(CONTENT_TYPE -> "application/json")))
        .withBody(Json.parse(""" { "customerId": 123456, "initialCredit": 58.78 } """))

      val response = controller.openCurrentAccount.apply(fakeRequest)

      status(response) mustBe OK
    }

    "return 404 for a no valid request" in {
      val controller =
        new AccountController(accountService, stubControllerComponents())

      val fakeRequest = FakeRequest(POST, "/account/current")
        .withHeaders(FakeHeaders(Seq(CONTENT_TYPE -> "application/json")))
        .withBody(Json.parse(""" { "customerId": 123456 } """))

      val response = controller.openCurrentAccount.apply(fakeRequest)

      status(response) mustBe BAD_REQUEST
    }
  }

}
