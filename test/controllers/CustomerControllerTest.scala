package controllers

import domain.CustomerDomainService
import helpers.ServicesBuilder.MockTransactionService
import infrastructure.InMemoryCustomerRepository
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.Configuration
import play.api.http.Status.{NOT_FOUND, OK}
import play.api.libs.json.Json
import play.api.test.Helpers.{
  contentAsString,
  contentType,
  defaultAwaitTimeout,
  status,
  stubControllerComponents
}
import play.api.test.{FakeRequest, Injecting}

import scala.concurrent.ExecutionContext.Implicits.global

class CustomerControllerTest extends PlaySpec with GuiceOneAppPerTest with Injecting {

  val testConfig: Configuration =
    Configuration.apply(("customers.data.path", "test/data/customers.json"))
  val customerRepository = new InMemoryCustomerRepository(testConfig)
  val mockTransactionService = new MockTransactionService
  val customerDomainService =
    new CustomerDomainService(customerRepository, mockTransactionService)

  "CustomerController GET /customers" should {

    "return all the customers as json" in {
      val controller =
        new CustomerController(customerDomainService, stubControllerComponents())
      val response = controller.getAllCustomers.apply(FakeRequest())

      status(response) mustBe OK
      contentType(response) mustBe Some("application/json")
      Json.parse(contentAsString(response)).result.isDefined mustBe true
    }

    "return customer by id as json" in {
      val controller =
        new CustomerController(customerDomainService, stubControllerComponents())
      val response =
        controller.getCustomerTransactionsSummary(123456).apply(FakeRequest())

      status(response) mustBe OK
      contentType(response) mustBe Some("application/json")

      contentAsString(response) mustBe """{"customerId":123456,"name":"Peter","surName":"Pan","balance":52,"transactions":[{"amount":52,"accountId":456,"dateTime":"1970-01-01T00:00:00Z"}]}"""
    }

    "return 404 when the customer id is not found" in {
      val controller =
        new CustomerController(customerDomainService, stubControllerComponents())
      val response =
        controller.getCustomerTransactionsSummary(555).apply(FakeRequest())

      status(response) mustBe NOT_FOUND
      contentType(response) mustBe Some("text/plain")

      contentAsString(response) mustBe "Customer id not found"
    }

  }
}
