package domain.external

case class TransactionServiceClientError(message: String)
