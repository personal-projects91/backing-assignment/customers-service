package domain.external

import domain.{CustomerId, Transaction}

import scala.concurrent.{ExecutionContext, Future}

trait TransactionServiceClient {
  def sendTransaction(transaction: Transaction)(implicit ex: ExecutionContext)
    : Future[Either[TransactionServiceClientError, Transaction]]
  def getTransactionsByCustomer(customerId: CustomerId)(implicit ex: ExecutionContext)
    : Future[Either[TransactionServiceClientError, List[Transaction]]]
}
