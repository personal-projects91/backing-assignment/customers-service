package domain.external

import domain.{Customer, CustomerId}

import scala.concurrent.Future

trait CustomerRepository {

  def customerById(customerId: CustomerId): Future[Option[Customer]]
  def allCustomers: Future[List[Customer]]

}
