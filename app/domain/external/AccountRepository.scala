package domain.external

import domain.Account

import scala.concurrent.Future

trait AccountRepository {
  def saveAccount(account: Account): Future[Either[AccountPersistenceError, Account]]
}
