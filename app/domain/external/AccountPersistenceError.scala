package domain.external

case class AccountPersistenceError(message: String)
