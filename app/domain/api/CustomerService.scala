package domain.api

import domain.{Customer, CustomerAggregate, CustomerId}

import scala.concurrent.{ExecutionContext, Future}

trait CustomerService {
  def getCustomerAggregate(customerId: CustomerId)(implicit executor: ExecutionContext)
    : Future[Either[CustomerHandlerError, Option[CustomerAggregate]]]
  def getAllCustomers: Future[List[Customer]]
}
