package domain.api

case class AccountHandlerError(message: String)
