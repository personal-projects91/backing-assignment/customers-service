package domain.api

import domain.Account

import scala.concurrent.{ExecutionContext, Future}

trait AccountService {
  def openAccount(account: Account)(
      implicit executor: ExecutionContext): Future[Either[AccountHandlerError, Account]]
}
