package domain.api

case class CustomerHandlerError(message: String)
