package domain

import scala.util.Random

case class Money(value: Double) extends AnyVal
case class AccountId(id: Long) extends AnyVal

sealed trait Account {
  val accountId: AccountId
  val customerId: CustomerId
}

case class CurrentAccount(accountId: AccountId,
                          customerId: CustomerId,
                          initialCredit: Money)
    extends Account {
  def generateTransaction: Boolean = initialCredit.value != 0
}

object CurrentAccount {
  def apply(customerId: CustomerId, initialCredit: Money)(
      randomUuid: () => Long = () => Random.nextLong()): CurrentAccount =
    CurrentAccount(
      AccountId(randomUuid()),
      customerId,
      initialCredit,
    )
}
