package domain

case class Balance(value: Double) extends AnyVal

case class CustomerAggregate(customer: Customer, transactions: List[Transaction]) {

  val balance: Money =
    transactions.foldLeft(Money(0))((acc, tx) => Money(acc.value + tx.amount.value))
}
