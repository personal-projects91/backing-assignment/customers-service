package domain

import cats.data.EitherT
import domain.api.{AccountHandlerError, AccountService}
import domain.external.{AccountRepository, CustomerRepository, TransactionServiceClient}
import javax.inject.Inject
import play.api.Logging

import scala.concurrent.{ExecutionContext, Future}

class AccountDomainService @Inject()(accountRepository: AccountRepository,
                                     customerRepository: CustomerRepository,
                                     transactionServiceClient: TransactionServiceClient)
    extends AccountService
    with Logging {

  def openAccount(account: Account)(implicit executor: ExecutionContext)
    : Future[Either[AccountHandlerError, Account]] = {

    (for {
      _ <- EitherT(findCustomer(account.customerId))
      accountCreated <- EitherT(saveAccount(account))
      _ <- EitherT(sendTransaction(account))
    } yield {
      accountCreated
    }).value
  }

  private def sendTransaction(account: Account)(implicit executor: ExecutionContext) = {
    account match {
      case newAccount: CurrentAccount if newAccount.generateTransaction =>
        transactionServiceClient
          .sendTransaction(
            Transaction(newAccount.customerId,
                        newAccount.accountId,
                        newAccount.initialCredit))
          .map(result =>
            result.left.map(error => {
              logger.error(s"Error sending transaction: $error")
              AccountHandlerError(s"Error sending transaction: ${error.message}")
            }))
      case _ => Future.successful(Right(None))
    }
  }

  private def saveAccount(account: Account)(implicit executor: ExecutionContext) = {
    accountRepository
      .saveAccount(account)
      .map(result =>
        result.left.map(error => {
          logger.error(s"Error handling account: $error")
          AccountHandlerError(
            s"Error returned by the account repository: ${error.message}")
        }))
  }

  private def findCustomer(customerId: CustomerId)(
      implicit executor: ExecutionContext) = {
    customerRepository
      .customerById(customerId)
      .map(result => {
        result.fold[Either[AccountHandlerError, Customer]](
          Left(AccountHandlerError(s"Customer id not found: ${customerId.id}")))(Right(_))
      })
  }
}
