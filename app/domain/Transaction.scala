package domain

import java.time.Instant
import java.util.UUID
case class TransactionId(id: UUID) extends AnyVal
case class Transaction(transactionId: Option[TransactionId],
                       customerId: CustomerId,
                       accountId: AccountId,
                       amount: Money,
                       dateTime: Option[Instant])

object Transaction {
  def apply(customerId: CustomerId, accountId: AccountId, amount: Money): Transaction = {
    new Transaction(None, customerId, accountId, amount, None)
  }
}
