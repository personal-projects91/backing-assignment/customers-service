package domain

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Reads, Writes}

case class CustomerId(id: Long) extends AnyVal

case class CustomerFullName(name: String, surName: String)

case class Customer(customerId: CustomerId, customerFullName: CustomerFullName)

object Customer {
  def apply(customerId: Long, name: String, surName: String): Customer =
    Customer(CustomerId(customerId), CustomerFullName(name, surName))

  def unapply(customer: Customer): (Long, String, String) =
    (customer.customerId.id,
     customer.customerFullName.name,
     customer.customerFullName.surName)

  implicit val jsonReads: Reads[Customer] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "name").read[String] and
      (JsPath \ "surName").read[String]
  )((id, name, surName) => Customer(id, name, surName))

  implicit val jsonWrites: Writes[Customer] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "name").write[String] and
      (JsPath \ "surName").write[String]
  )(Customer.unapply _)
}
