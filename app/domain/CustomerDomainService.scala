package domain

import domain.api.{CustomerHandlerError, CustomerService}
import domain.external.{CustomerRepository, TransactionServiceClient}
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class CustomerDomainService @Inject()(
    val customerRepository: CustomerRepository,
    val transactionServiceClient: TransactionServiceClient)
    extends CustomerService {

  override def getCustomerAggregate(customerId: CustomerId)(
      implicit executor: ExecutionContext)
    : Future[Either[CustomerHandlerError, Option[CustomerAggregate]]] = {

    val result = customerRepository.customerById(customerId).flatMap {
      case Some(customer) =>
        transactionServiceClient
          .getTransactionsByCustomer(customer.customerId)
          .map(_.map(transactions => {
            Some(CustomerAggregate(customer, transactions))
          }))
      case None => Future.successful(Right(None))
    }

    result.map(
      _.left.map(error =>
        CustomerHandlerError(
          s"An error occurred collecting the customer aggregate ${error.message}")))
  }

  override def getAllCustomers: Future[List[Customer]] = customerRepository.allCustomers
}
