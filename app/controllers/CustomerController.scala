package controllers

import controllers.dtos.CustomerTransactionsViewDTO
import domain.CustomerId
import domain.api.CustomerService
import javax.inject.Inject
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents, Request}

import scala.concurrent.ExecutionContext

class CustomerController @Inject()(
    val customerService: CustomerService,
    val controllerComponents: ControllerComponents)(implicit ec: ExecutionContext)
    extends BaseController {

  def getAllCustomers: Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      customerService.getAllCustomers
        .map(
          result => Ok(Json.toJson(result))
        )
  }

  def getCustomerTransactionsSummary(customerId: Long): Action[AnyContent] =
    Action.async { implicit request: Request[AnyContent] =>
      customerService
        .getCustomerAggregate(CustomerId(customerId))
        .map {
          case Left(_) => InternalServerError("Error getting customer data")
          case Right(customerOption) =>
            val responseDTO =
              customerOption.map(CustomerTransactionsViewDTO.fromCustomerAggregate)
            responseDTO.fold(NotFound("Customer id not found"))(c => Ok(Json.toJson(c)))
        }
    }

}
