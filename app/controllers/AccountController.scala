package controllers

import controllers.dtos.{AccountResponseDTO, CurrentAccountDTO}
import domain.AccountDomainService
import javax.inject.Inject
import play.api.libs.json.{JsError, JsValue, Json}
import play.api.mvc.{Action, BaseController, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

class AccountController @Inject()(
    val accountService: AccountDomainService,
    val controllerComponents: ControllerComponents)(implicit ec: ExecutionContext)
    extends BaseController {

  def openCurrentAccount: Action[JsValue] =
    Action(parse.json).async { request =>
      val accountJson = request.body.validate[CurrentAccountDTO]
      accountJson.fold(
        errors => {
          Future.successful(BadRequest(Json.obj("message" -> JsError.toJson(errors))))
        },
        accountDTO => {
          val transaction = accountDTO.toAccount
          accountService.openAccount(transaction).map {
            case Left(_)       => InternalServerError("Error opening the account")
            case Right(result) => Ok(Json.toJson(AccountResponseDTO.fromAccount(result)))
          }
        }
      )
    }

}
