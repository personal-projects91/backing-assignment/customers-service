package controllers.dtos

import domain.{Account, CurrentAccount, CustomerId, Money}
import play.api.libs.json.{Json, Reads}

case class CurrentAccountDTO(customerId: Long, initialCredit: Double) {
  def toAccount: Account = {
    CurrentAccount(CustomerId(customerId), Money(initialCredit))()
  }
}

object CurrentAccountDTO {
  implicit val jsonReads: Reads[CurrentAccountDTO] = Json.reads[CurrentAccountDTO]
}
