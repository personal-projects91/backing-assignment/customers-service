package controllers.dtos

import domain.CustomerAggregate
import play.api.libs.json.{Json, Writes}

case class TransactionSummaryDTO(amount: Double, accountId: Long, dateTime: String)
case class CustomerTransactionsViewDTO(customerId: Long,
                                       name: String,
                                       surName: String,
                                       balance: Double,
                                       transactions: List[TransactionSummaryDTO])

object CustomerTransactionsViewDTO {
  implicit val jsonTransactionWrites: Writes[TransactionSummaryDTO] =
    Json.writes[TransactionSummaryDTO]

  implicit val jsonWrites: Writes[CustomerTransactionsViewDTO] =
    Json.writes[CustomerTransactionsViewDTO]

  def fromCustomerAggregate(
      customerAggregate: CustomerAggregate): CustomerTransactionsViewDTO = {
    val transactions = customerAggregate.transactions.map(
      t =>
        TransactionSummaryDTO(t.amount.value,
                              t.accountId.id,
                              t.dateTime.map(_.toString).getOrElse("")))
    CustomerTransactionsViewDTO(
      customerAggregate.customer.customerId.id,
      customerAggregate.customer.customerFullName.name,
      customerAggregate.customer.customerFullName.surName,
      customerAggregate.balance.value,
      transactions
    )

  }
}
