package controllers.dtos

import domain.Account
import play.api.libs.json.{Json, Writes}

case class AccountResponseDTO(accountId: Long)

object AccountResponseDTO {
  implicit val jsonWrites: Writes[AccountResponseDTO] = Json.writes[AccountResponseDTO]
  def fromAccount(account: Account): AccountResponseDTO =
    AccountResponseDTO(account.accountId.id)
}
