import com.google.inject.AbstractModule
import domain.api.{AccountService, CustomerService}
import domain.{AccountDomainService, CustomerDomainService}
import domain.external.{AccountRepository, CustomerRepository, TransactionServiceClient}
import infrastructure.http.TransactionHttpClient
import infrastructure.{InMemoryAccountRepository, InMemoryCustomerRepository}

class ApplicationModule extends AbstractModule {
  override def configure() = {
    bind(classOf[CustomerRepository])
      .to(classOf[InMemoryCustomerRepository])

    bind(classOf[CustomerService])
      .to(classOf[CustomerDomainService])

    bind(classOf[AccountService])
      .to(classOf[AccountDomainService])

    bind(classOf[AccountRepository])
      .to(classOf[InMemoryAccountRepository])

    bind(classOf[TransactionServiceClient])
      .to(classOf[TransactionHttpClient])
  }
}
