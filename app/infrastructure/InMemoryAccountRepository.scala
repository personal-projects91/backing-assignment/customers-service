package infrastructure

import domain.Account
import domain.external.{AccountPersistenceError, AccountRepository}
import javax.inject.Singleton
import play.api.Logging

import scala.collection.concurrent.{Map, TrieMap}
import scala.concurrent.Future
import scala.util.Try

@Singleton
class InMemoryAccountRepository extends AccountRepository with Logging {

  private val accountsByIdView: Map[Long, Account] = TrieMap.empty

  override def saveAccount(
      account: Account): Future[Either[AccountPersistenceError, Account]] =
    Future.successful {
      Try {
        accountsByIdView.addOne((account.accountId.id, account))
      } fold (error => {
        logger.error("An error occurred saving the account", error)
        Left(AccountPersistenceError(error.getMessage))
      },
      _ => Right(account))
    }
}
