package infrastructure.http.dtos

import java.time.Instant
import java.util.UUID

import domain.{AccountId, CustomerId, Money, Transaction, TransactionId}
import play.api.libs.json.{Json, Reads}

case class TransactionDTO(transactionId: String,
                          customerId: Long,
                          accountId: Long,
                          amount: Double,
                          dateTime: Long) {

  def toTransaction: Transaction =
    Transaction(Some(TransactionId(UUID.fromString(transactionId))),
                CustomerId(customerId),
                AccountId(accountId),
                Money(amount),
                Some(Instant.ofEpochMilli(dateTime)))
}

object TransactionDTO {

  implicit val jsonReads: Reads[TransactionDTO] = Json.reads[TransactionDTO]

}
