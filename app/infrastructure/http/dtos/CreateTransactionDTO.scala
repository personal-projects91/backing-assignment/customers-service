package infrastructure.http.dtos

import domain.Transaction
import play.api.libs.json.{Json, Writes}

case class CreateTransactionDTO(customerId: Long, accountId: Long, amount: Double)

object CreateTransactionDTO {
  implicit val jsonWrites: Writes[CreateTransactionDTO] =
    Json.writes[CreateTransactionDTO]

  def formTransaction(transaction: Transaction): CreateTransactionDTO = {
    CreateTransactionDTO(transaction.customerId.id,
                         transaction.accountId.id,
                         transaction.amount.value)
  }
}
