package infrastructure.http

import akka.http.scaladsl.model.StatusCode
import domain.{CustomerId, Transaction}
import domain.external.{TransactionServiceClient, TransactionServiceClientError}
import infrastructure.http.dtos.{CreateTransactionDTO, TransactionDTO}
import javax.inject.Inject
import play.api.libs.json.{Json, Reads}
import play.api.{Configuration, Logging}
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class TransactionHttpClient @Inject()(ws: WSClient, config: Configuration)
    extends TransactionServiceClient
    with Logging {

  private val GET_TRANSACTIONS_ENDPOINT =
    config.get[String]("clients.transactions.byCustomer.endpoint")
  private val SAVE_TRANSACTIONS_ENDPOINT =
    config.get[String]("clients.transactions.save.endpoint")

  override def sendTransaction(transaction: Transaction)(implicit ex: ExecutionContext)
    : Future[Either[TransactionServiceClientError, Transaction]] = {

    val transactionRequest = CreateTransactionDTO.formTransaction(transaction)

    ws.url(SAVE_TRANSACTIONS_ENDPOINT)
      .addHttpHeaders("Accept" -> "application/json")
      .post(Json.toJson(transactionRequest))
      .map(response => responseMapping[TransactionDTO](response).map(_.toTransaction))

  }

  override def getTransactionsByCustomer(customerId: CustomerId)(
      implicit ex: ExecutionContext)
    : Future[Either[TransactionServiceClientError, List[Transaction]]] = {
    ws.url(GET_TRANSACTIONS_ENDPOINT.format(customerId.id))
      .addHttpHeaders("Accept" -> "application/json")
      .get()
      .map(response =>
        responseMapping[List[TransactionDTO]](response).map(_.map(_.toTransaction)))

  }

  def responseMapping[A](response: WSResponse)(
      implicit rds: Reads[A]): Either[TransactionServiceClientError, A] = {

    val (status, body) = (StatusCode.int2StatusCode(response.status), response.body)

    (status, body) match {
      case (status, body) if status.isSuccess() && body.nonEmpty =>
        Try(response.json.as[A]).toEither.left.map(error => {
          logger.error(s"Error parsing json response: ${error.getMessage}", error)
          TransactionServiceClientError(error.getMessage)
        })
      case (status, _) =>
        logger.error(s"HTTP request rejected with status code: $status")
        Left(
          TransactionServiceClientError(
            s"HTTP request rejected with status code: $status"))
    }
  }
}
