package infrastructure

import domain.external.CustomerRepository
import domain.{Customer, CustomerId}
import javax.inject.{Inject, Singleton}
import play.api.{Configuration, Logging}
import play.api.libs.json.Json

import scala.concurrent.Future
import scala.io.Source
import scala.util.Try

@Singleton
class InMemoryCustomerRepository @Inject()(config: Configuration)
    extends CustomerRepository
    with Logging {

  private val CUSTOMER_DATA_PATH =
    config.get[String]("customers.data.path")

  //Customers in memory
  private val customers: Map[Long, Customer] = loadCustomers(CUSTOMER_DATA_PATH)

  override def customerById(customerId: CustomerId): Future[Option[Customer]] =
    Future.successful(customers.get(customerId.id))

  override def allCustomers: Future[List[Customer]] =
    Future.successful(customers.toList.map(_._2))

  def loadCustomers(dataPath: String): Map[Long, Customer] = {
    val jsonFile = Source.fromFile(dataPath)
    Try {
      Json.parse(jsonFile.mkString).as[List[Customer]]
    }.fold(
      error => {
        logger.error("An error occurred loading the customers", error)
        Map.empty
      },
      customers => {
        customers.foldLeft(Map.empty[Long, Customer])((acc, c) =>
          acc + (c.customerId.id -> c))
      }
    )
  }
}
