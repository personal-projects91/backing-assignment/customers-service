# Customer service

Service to get customers transactions and open accounts

### Service Dependencies

[Transaction Service](https://gitlab.com/personal-projects91/backing-assignment/transaction-service) running locally on port 9002

### Running the server locally

Install [SBT](https://www.scala-sbt.org/) and execute: 
```
sbt run -Dhttp.port=9001
```

### API documentation

Once the service is running the [Swagger doc](http://localhost:9001/docs/swagger-ui/index.html?url=/assets/swagger.json) is available with the endpoints and schema details 

#### Using main endpoints
- To get the customer ids saved in memory you can call the endpoint `GET /customer`. Curl command:
```
curl -X GET "http://localhost:9001/customers" -H  "accept: application/json"
```
- Using some of those ids you can call the endpoint `POST /account/current` to open a current account. Curl command:
```
curl -X POST "http://localhost:9001/account/current" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"customerId\":123456,\"initialCredit\":58.9}"
```
- To get the customer transactions summary use the endpoint `GET /customer/{id}/transactions-summary`  
```
curl -X GET "http://localhost:9001/customer/123456/transactions-summary" -H  "accept: application/json"
```

### Running test
```
sbt test
```

### Tools
- Play Framework (scala)
- sbt (plugins: sbt-scoverage - sbt-scalafmt)
